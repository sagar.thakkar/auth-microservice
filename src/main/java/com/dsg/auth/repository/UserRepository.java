package com.dsg.auth.repository;

import com.dsg.auth.model.User;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.math.BigInteger;

@Repository
public interface UserRepository extends ReactiveMongoRepository<User, BigInteger> {

    User findUserByUsername(String username);

    Flux<User> findAll();
}
