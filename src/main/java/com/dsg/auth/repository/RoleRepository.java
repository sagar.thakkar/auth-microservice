package com.dsg.auth.repository;

import com.dsg.auth.model.Role;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.math.BigInteger;

@Repository
public interface RoleRepository extends ReactiveMongoRepository<Role, BigInteger> {
    Mono<Role> findByAuthority(String role);
}
