package com.dsg.auth.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public interface IUserService {

    UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException;
}
