package com.dsg.auth.service.serviceImpl;

import com.dsg.auth.model.User;
import com.dsg.auth.repository.UserRepository;
import com.dsg.auth.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class UserService implements IUserService {

    @Autowired
    private UserRepository userRepository;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final User user = userRepository.findUserByUsername(username);
        return new org.springframework.security.core.userdetails.User(username, user.getPassword(), user.getAuthorities());
    }
}
