package com.dsg.auth.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;

import java.math.BigInteger;

@Data
@Document(collection = "auth-user.role")
public class Role implements GrantedAuthority {

    @Id
    private BigInteger id;

    private String role;

    @Override
    public String getAuthority() {
        return role;
    }

    public Role(String role) {
        this.role = role;
    }

    public Role(BigInteger id, String role) {
        this.id = id;
        this.role = role;
    }
}
