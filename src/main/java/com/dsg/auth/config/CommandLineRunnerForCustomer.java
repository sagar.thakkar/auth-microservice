package com.dsg.auth.config;

import com.dsg.auth.model.Role;
import com.dsg.auth.model.User;
import com.dsg.auth.repository.RoleRepository;
import com.dsg.auth.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Example;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Configuration
@Slf4j
public class CommandLineRunnerForCustomer implements CommandLineRunner {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;

    @Override
    public void run(String... args)  {


        List<Role> roles = new ArrayList<>();
        roleRepository.findAll(Example.of(new Role(new BigInteger("an"),"ADMIN"))).subscribe(roles::add);
        System.out.println(roles.toString());
        System.out.println(roles.isEmpty());
        if (roles.isEmpty()) {
//            roles.add(new Role("ADMIN"));
//            roles.add(new Role("USER"));
            roleRepository.insert(new Role("ADMIN"));
        }


//        log.warn(Objects.requireNonNull(userRepository.findAll().blockFirst()).toString());
   /*     if (!(userRepository.findAll().blockFirst().getId() != null)) {
            List<Role>  roleList = new ArrayList<>();
            roleList.add(roleRepository.findByAuthority("ADMIN"));
            log.error(roleList.toString());
            *//*userRepository.save(new User(
                    "thakkarsagar12",
                    "thakkarsagar12@gmail.com",
                    new BCryptPasswordEncoder().encode("password"),
                    roleList,
                    true,
                    true,
                    true,
                    true
            ));*//*
        }*/
    }
}
